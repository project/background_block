# Background Block

This module is used to add or change block background color.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/background_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/background_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit the block configuration page at 
   `Administration » Structure » Block layout` 
   and click on the Configure link for a block.

2. Enter the color code in the field provided and save the block.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat - [radheymkumar](https://www.drupal.org/u/radheymkumar)